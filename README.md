Introduction
------------

This is an uber-repo for random blender addon experiments, tests, dead-ends, and amazingly useful tools

floating sliders
----------------
adds simple sliders for selected bone properties in the viewport, a proof of concept, my personal feeling is that this isn't exactly production ready as is.

picker
------
Creates a graphical 'picker' window in the 3D viewport. This is a barebones skeleton, and it neatly gets the layers from an .svg file. With fleshing out, this could work fine for production in current blender.

typewriter text
---------------
Allows creating an animated 'typing' effect for text blocks in blender

text-fx
-------
A more elaborate stack-based effects addon for text objects, including the above typing effect. I've used this in several productions, though the UI is quite strange.